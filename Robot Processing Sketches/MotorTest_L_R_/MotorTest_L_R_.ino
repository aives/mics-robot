// Adafruit Motor shield library
// copyright Adafruit Industries LLC, 2009
// this code is public domain, enjoy!

#include <AFMotor.h>

AF_DCMotor motorL(4);
AF_DCMotor motorR(3);

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps

  // turn on motor
  motorR.setSpeed(200);
  motorL.setSpeed(200);
 
  motorR.run(RELEASE);
  motorL.run(RELEASE);
}

void loop() {
  uint8_t i;
  
  motorL.run(FORWARD);
  for (i=0; i<255; i++) {
    motorL.setSpeed(i);  
    delay(10);
 }
 
  for (i=255; i!=0; i--) {
    motorL.setSpeed(i);  
    delay(10);
 }
  
  Serial.print("tock");

  motorR.run(FORWARD);
  for (i=0; i<255; i++) {
    motorR.setSpeed(i);  
    delay(10);
 }
 
  for (i=255; i!=0; i--) {
    motorR.setSpeed(i);  
    delay(10);
 }
  
  motorR.run(FORWARD);
  motorL.run(FORWARD);
  for (i=0; i<255; i++) {
    motorR.setSpeed(i);  
    motorL.setSpeed(i);  
    delay(10);
 }
 
  for (i=255; i!=0; i--) {
    motorR.setSpeed(i);  
    motorL.setSpeed(i);  
    delay(10);
 }
  

  Serial.print("tech");
  motorR.run(RELEASE);
  motorL.run(RELEASE);
  delay(1000);
}

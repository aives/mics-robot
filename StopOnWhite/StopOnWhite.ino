#include <AFMotor.h>
#include <Servo.h> 
/********************************
Stops on a drastic light/dark change under the front reflectance sensor.

By Alex Ives
Last Modified, 4/16/2013
*********************************/


// Motor shield declarations
AF_DCMotor motorL(4); // Define the left motor
AF_DCMotor motorR(3); // Define the right motor

// Servo
Servo myservo;

// Sensor Declarations
int qtrFrontPin = A1; // Analog pin connected to Front QTR-1A Reflectance sensor
int qtrBackPin = A2; // Analog pin connected to back QTR-1A Reflectance sensor

// LED Pins
int ledPin = 13; // Holds the pin for the led

void setup() {
  
  // initialize servo
  myservo.attach(9);
  
  // turn on motor
  motorR.setSpeed(200);
  motorL.setSpeed(200);
  motorR.run(RELEASE);
  motorL.run(RELEASE);
  
  // initialize led
  pinMode(ledPin, OUTPUT);
}


void loop() {
  delay(10);
  uint8_t i;
  motorL.run(FORWARD);
  motorR.run(FORWARD);
  int sensorValue = analogRead(qtrFrontPin);
  int startValue = sensorValue;
  while (startValue < (sensorValue+10) || startValue > (sensorValue+10)) {
    delay(10);
    sensorValue = analogRead(qtrFrontPin);
  }
  if(startValue < sensorValue) {
    digitalWrite(ledPin, HIGH);
    delay(5000);
    digitalWrite(ledPin, LOW);
  }
  delay(5000);
  motorL.setSpeed(0);
  motorR.setSpeed(0);
}
